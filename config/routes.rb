Rails.application.routes.draw do
  get :about, to: 'home#about', as: :about
  get :services, to: 'home#services', as: :services
  get :team, to: 'home#team', as: :team
  get :index, to: 'home#index'
  get :contact, to: 'home#contact', as: :contact
  post :contact_submission, to: 'home#contact_submission'

  root 'home#index'
end
