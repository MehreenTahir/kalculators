class HomeController < ApplicationController
  def index
  end

  def about
  end

  def services
  end

  def team
  end

  def contact
  end

  def contact_submission
    if params[:message].present? || params[:name].present?
      @message = 'Your message has been sent succesfully.'
      ContactMailer.contact_notification(params).deliver_now
    else
      @message = 'Message not sent, Please add your name or message.'
    end
  end
end
