class ContactMailer < ApplicationMailer
  def contact_notification(params)
    @data = {
      name: params[:name],
      email: params[:email],
      message: params[:message]
    }
    mail to: 'thekalculators@gmail.com', subject: 'New Message Recived'
  end
end
