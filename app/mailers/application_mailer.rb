class ApplicationMailer < ActionMailer::Base
  default from: 'web@thekalculators.com.au'
  layout 'mailer'
end
