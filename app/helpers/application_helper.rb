module ApplicationHelper
  def custom_body_class
    ['about', 'contact'].include?(params[:action]) ? '' : "service-single-page sub-page" 
  end

  def footer_class
    ['about', 'contact'].include?(params[:action]) ? 'padding-footer' : ''
  end

  def active_class(item)
    'active' if params[:action].to_sym == item
  end
end
