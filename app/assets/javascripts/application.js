// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require base/bootstrap.min.js
//= require rs-plugin/jquery.themepunch.tools.min.js
//= require rs-plugin/jquery.themepunch.revolution.min.js
//= require base/jquery.inview.min.js
//= require base/wow.js
//= require base/lightbox.js
//= require owl-carrosel/owl.carousel.min.js
//= require base/script.js
//= require_tree .
