bind_service_types = ->
  service_type = window.location.search.split('?type=')[1]
  return if ['taxation', 'finance', 'super', 'payroll', 'software'].indexOf(service_type) <= -1

  $("##{service_type} a").click()
  $(window).scrollTop($('.working-section').offset().top)

$ ->
  bind_service_types()
